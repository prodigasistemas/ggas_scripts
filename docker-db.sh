#!/bin/bash

JOB_NAME=$1
WORKSPACE=$2
SSH_PORT=$3
DB_PORT=$4

export GGAS_PATH=$WORKSPACE
export ANSIBLE_HOST_KEY_CHECKING=False

docker network create ggas-net

docker rm -f db-$JOB_NAME

docker run --name db-$JOB_NAME -d -p "$SSH_PORT:22" -p "$DB_PORT:1521" --network=ggas-net wnameless/oracle-xe-11g:16.04

sleep 30

ANSIBLE_HOSTS_FILE=/tmp/.ansible-hosts-$JOB_NAME

cp /opt/ggas_dev_setup/database/hosts $ANSIBLE_HOSTS_FILE

sed -i "s/ansible_port=2222/ansible_port=$SSH_PORT/g" $ANSIBLE_HOSTS_FILE

ansible-playbook -i $ANSIBLE_HOSTS_FILE /opt/ggas_dev_setup/database/app.yml

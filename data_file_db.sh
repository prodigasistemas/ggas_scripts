#!/bin/bash

FILE=sql/GGAS_SCRIPT_INICIAL_ORACLE_01_ROLES.sql

FIND='DATAFILE'\''C:\\oracle\\product\\10.2.0\\oradata\\GGAS_DADOS01.DBF'\'''
REPLACE='DATAFILE'\''/u01/app/oracle/oradata/XE/GGAS_DADOS01.DBF'\'''

sed -i "/$FIND/ a $REPLACE" $FILE

sed -i "s|$FIND|--$FIND|g" $FILE

FIND='DATAFILE'\''C:\\oracle\\product\\10.2.0\\oradata\\GGAS_INDEX01.DBF'\'''
REPLACE='DATAFILE'\''/u01/app/oracle/oradata/XE/GGAS_INDEX01.DBF'\'''

sed -i "/$FIND/ a $REPLACE" $FILE

sed -i "s|$FIND|--$FIND|g" $FILE

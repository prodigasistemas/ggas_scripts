#!/bin/bash
ps -ef | grep apache-tomcat | grep -v grep | awk '{print $2}' | xargs kill

cp /var/lib/jenkins/workspace/$1/build/libs/*.war /opt/apache-tomcat-7.0.64/webapps/GGAS.war

WORKSPACE_DIR=$2

export JAVA_OPTS="-javaagent:/opt/xrebel/xrebel.jar $JAVA_OPTS"

/opt/apache-tomcat-7.0.64/bin/./startup.sh

sleep 5m


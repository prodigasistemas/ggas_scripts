#!/bin/bash
aFIND="$(ps -ef | grep apache-tomcat | grep -v grep | awk '{print $2}' | wc -l)"
echo $aFIND
if [ $aFIND -gt 0 ];
then
	ps -ef | grep apache-tomcat | grep -v grep | awk '{print $2}' | xargs kill 
fi

aFIND="$(ps -ef | grep Xvfb | grep -v grep | awk '{print $2}' | wc -l)"
echo $aFIND
if [ $aFIND -gt 0 ];
then
	ps -ef | grep Xvfb | grep -v grep | awk '{print $2}' | xargs kill
fi
#!/bin/bash

JOB_NAME=$1
WORKSPACE=$2
DISPLAY_NUMBER=$3
SSH_PORT=$4
ORACLE_URL=$5
ORACLE_PORT=$6
TOMCAT_PORT=$7
GRADLE_BUILD_PARAMS=$8

export DISPLAY=:$DISPLAY_NUMBER

cd $WORKSPACE

/opt/ggas_scripts/kill_process.sh $JOB_NAME $DISPLAY_NUMBER

/opt/ggas_scripts/cleanup.sh $JOB_NAME $WORKSPACE

/opt/ggas_scripts/docker-db.sh $JOB_NAME $WORKSPACE $SSH_PORT $ORACLE_PORT

/opt/ggas_scripts/cp_properties.sh $JOB_NAME $WORKSPACE $ORACLE_URL $ORACLE_PORT $TOMCAT_PORT

./gradlew clean migration war --rerun-tasks

/opt/ggas_scripts/deploy.sh $JOB_NAME $WORKSPACE $DISPLAY_NUMBER &

./gradlew build $GRADLE_BUILD_PARAMS

[ $? -eq 0 ] && /opt/apache/tomcat/$JOB_NAME/bin/shutdown.sh

#!/bin/bash

JOB_NAME=$1
WORKSPACE_DIR=$2
DISPLAY_NUMBER=$3

cp $WORKSPACE_DIR/build/libs/*.war /opt/apache/tomcat/$JOB_NAME/webapps/ggas.war

export CATALINA_OPTS="-XX:MaxPermSize=512m"
#export DISPLAY=:$DISPLAY_NUMBER
export JAVA_OPTS="-server -Xmx4G" # -Dwebdriver.gecko.driver=/opt/geckodriver"

nohup Xvfb :$DISPLAY_NUMBER -screen 0 1600x1200x16 -nolisten tcp &

#ruby headless.rb

export JAVA_OPTS="-javaagent:/opt/jacoco/jacocoagent.jar=destfile=${WORKSPACE_DIR}/build/jacoco/selenium.exec,append=false $JAVA_OPTS"
#export JAVA_OPTS="-javaagent:/opt/xrebel/xrebel.jar $JAVA_OPTS"

/opt/apache/tomcat/$JOB_NAME/bin/startup.sh

echo ".............."

sleep 5m

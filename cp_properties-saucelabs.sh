#!/bin/bash

cp /opt/ggas_scripts/properties/$1/var/lib/jenkins/workspace/$1/src/main/resources/hibernate.properties
cp /opt/ggas_scripts/properties/$1/var/lib/jenkins/workspace/$1/src/main/resources/hibernate-test.properties
cp /opt/ggas_scripts/properties/selenium-saucelabs.properties /var/lib/jenkins/workspace/$1/src/main/resources/selenium.properties

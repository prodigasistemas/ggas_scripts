#!/bin/bash

JOB_NAME=$1
WORKSPACE=$2
ORACLE_URL=$3
ORACLE_PORT=$4
TOMCAT_PORT=$5

SOURCE=/opt/ggas_scripts/properties
DESTINATION=$WORKSPACE/src/main/resources

cp $SOURCE/hibernate.properties $DESTINATION/hibernate.properties
cp $SOURCE/hibernate.properties $DESTINATION/hibernate-test.properties
cp $SOURCE/selenium.properties $DESTINATION/selenium.properties

sed -i "s/@_URL_:_PORT_/@$ORACLE_URL:$ORACLE_PORT/g" $DESTINATION/hibernate.properties
sed -i "s/@_URL_:_PORT_/@$ORACLE_URL:$ORACLE_PORT/g" $DESTINATION/hibernate-test.properties
sed -i "s/_PORT_/$TOMCAT_PORT/g" $DESTINATION/selenium.properties

#!/bin/bash

JOB_NAME=$1
DISPLAY_NUMBER=$2

docker rm -f db-$JOB_NAME #2> /dev/null

TOMCAT_PID=$(ps -ef | grep "apache/tomcat/$JOB_NAME" | grep -v grep | awk '{print $2}')

[ -n "$TOMCAT_PID" ] && kill -9 "$TOMCAT_PID"

XVFB_PID=$(ps -ef | grep "Xvfb :$DISPLAY_NUMBER" | grep -v grep | awk '{print $2}')

[ -n "$XVFB_PID" ] && kill -9 "$XVFB_PID"

date

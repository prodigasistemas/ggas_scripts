#!/bin/bash

JOB_NAME=$1
WORKSPACE_DIR=$2
DISPLAY_NUMBER=$3
TOMCAT_PORT=$4

/opt/ggas_scripts/kill_process.sh $JOB_NAME $DISPLAY_NUMBER

mv $WORKSPACE_DIR/build/libs/*.war $WORKSPACE_DIR/build/libs/ggas.war

nohup Xvfb :$DISPLAY_NUMBER -screen 0 1600x1200x16 -nolisten tcp &

#export JAVA_OPTS="-javaagent:/opt/jacoco/jacocoagent.jar=destfile=${WORKSPACE_DIR}/build/jacoco/selenium.exec,append=false $JAVA_OPTS"

cd $WORKSPACE_DIR/build/libs

docker rm -f app-$JOB_NAME

docker build -f /opt/ggas_scripts/Dockerfile -t app-$JOB_NAME .

docker run -it -d --rm -p $TOMCAT_PORT:8080 --name=app-$JOB_NAME --network=ggas-net -v $WORKSPACE_DIR/jacoco/selenium:/tmp/jacoco app-$JOB_NAME

sleep 5m

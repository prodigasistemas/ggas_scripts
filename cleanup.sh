#!/bin/bash

JOB_NAME=$1
WORKSPACE=$2

rm -f /opt/apache/tomcat/$JOB_NAME/logs/*
rm -Rf /opt/apache/tomcat/$JOB_NAME/temp/*
rm -Rf /opt/apache/tomcat/JOB_NAME/webapps/GGAS
rm -Rf /opt/apache/tomcat/$JOB_NAME/webapps/GGAS.war
rm -Rf $WORKSPACE/build/libs/*.war
rm -Rf $WORKSPACE/$JOB/GGAS
rm ~/UpdateDb_*.log

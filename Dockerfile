FROM ggas/tomcat8

RUN mkdir /tmp/jacoco

ENV CATALINA_OPTS='-server -Xms512m -Xmx3g'
ENV JPDA_ADDRESS="8009"
ENV JPDA_TRANSPORT="dt_socket"
ENV JAVA_OPTS="-javaagent:/opt/jacoco/jacocoagent.jar=destfile=/tmp/jacoco/selenium.exec,append=false -server -Xmx2g"

VOLUME /tmp/jacoco

COPY ggas.war /opt/tomcat/webapps

ENTRYPOINT ["/opt/tomcat/bin/catalina.sh", "jpda", "run"]
